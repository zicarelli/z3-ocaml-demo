# OCaml example usage of Z3 API

This repository contains a toy OCaml program `max` written to learn the Z3 API.

## Building

First install install the bindings by running `opam install z3`, then run `make`.

## Example usage

```
$ ./max 44
(declare-fun k () Int)
(declare-fun d () Int)
(assert (= k 44))
(assert (or (= d 1) (= d 2) (= d 4) (= d 8) (= d 16) (= d 32)))
(assert (= (mod k d) 0))
(maximize d)
(check-sat)
Status: satisfiable!  Given k=44, maximized d=4.
```
