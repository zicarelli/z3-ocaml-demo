OCB_FLAGS  = -use-ocamlfind -use-menhir -I src -no-links
OCB = ocamlbuild $(OCB_FLAGS)

BUILD = _build/src

all: max

clean:
	$(OCB) -clean

max: src/max.ml
	$(OCB) -package z3 -tag thread max.native
	cp $(BUILD)/max.native max

.PHONY: all clean
