let () =
  let k_val : int = int_of_string Sys.argv.(1) in
  let ctx = Z3.mk_context [] in (* most Z3 API calls require a context *)

  (* declarations: *)
  let k = Z3.Arithmetic.Integer.mk_const ctx (Z3.Symbol.mk_string ctx "k") in
  let d = Z3.Arithmetic.Integer.mk_const ctx (Z3.Symbol.mk_string ctx "d") in
  let num (n : int) = Z3.Arithmetic.Integer.mk_numeral_i ctx n in

  (* boolean: (= k <k_val>)  *)
  let k_eq = Z3.Boolean.mk_eq ctx k (num k_val) in

  (* boolean: (or (= d 1) (= d 2) (= d 4) ... (= d 32))  *)
  let d_eq = Z3.Boolean.mk_or ctx
    ([1; 2; 4; 8; 16; 32] |> List.map (fun n ->
      Z3.Boolean.mk_eq ctx d (num n))) in

  (* boolean: (= (mod k d) 0)  *)
  let d_divides_k = Z3.Boolean.mk_eq ctx
    (Z3.Arithmetic.Integer.mk_mod ctx k d) (num 0) in

  (* optimization: *)
  let opt = Z3.Optimize.mk_opt ctx in
  Z3.Optimize.add opt [ k_eq; d_eq; d_divides_k ]; (* assert each boolean *)
  let handle = Z3.Optimize.maximize opt d in       (* maximize d *)
  print_string (Z3.Optimize.to_string opt);        (* prints out SMT-LIB *)
  let status : string = Z3.Solver.string_of_status (Z3.Optimize.check opt) in
  let d_max : int =
    Z.to_int (Z3.Arithmetic.Integer.get_big_int (Z3.Optimize.get_upper handle))
  in
  Printf.printf "Status: %s!  Given k=%d, maximized d=%d.\n" status k_val d_max
